package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public enum BrowserFactory {
    CHROME {
        public WebDriver create(){
            System.setProperty("webdriver.chrome.driver", "C:\\Drivers\\chromedriver_win32\\chromedriver.exe");
            return new ChromeDriver();
        }
    },
    IE {
        public WebDriver create(){
            System.setProperty("webdriver.ie.driver", "C:\\Drivers\\IEDriverServer_Win32_3.150.1\\IEDriverServer.exe");
            return new InternetExplorerDriver();
        }
    },
    FIREFOX {
        public WebDriver create() {
            System.setProperty("webdriver.gecko.driver","C:\\Drivers\\geckodriver-v0.26.0-win64\\geckodriver.exe");
            return new FirefoxDriver();
        }
    },
    EDGE {
        public WebDriver create() {
            System.setProperty("webdriver.edge.driver", "C:\\Drivers\\edgedriver_win64\\msedgedriver.exe");
            return new EdgeDriver();
        }
    };

    public WebDriver create(){
        return null;
    }
}


